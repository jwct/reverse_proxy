#!/bin/bash
read -p "User name:"
USER=$REPLY
read -p "Password:"
PASSWD=$REPLY

if [ -z ${USER:+x} ];
then 
    echo "No user"
    exit 1
fi

if [ -z ${PASSWD} ];
then
    echo "No password"
    exit 1
fi

ENC_PASSWD=`openssl passwd  -crypt ${PASSWD}`

echo "${USER}:${ENC_PASSWD}" >> conf.d/htaccess
