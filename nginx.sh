docker run \
    --rm \
    -v "`pwd`/conf.d:/etc/nginx/conf.d" \
    -v "`pwd`/src:/home/appuser" \
    -p 80:80 \
    -p 443:443 \
    --name reverse_proxy \
    --network=br0 \
    --ip=172.28.5.1 \
    -u root \
    -w /home/appuser \
    reverse_proxy \
    /home/appuser/entrypoint.sh 
